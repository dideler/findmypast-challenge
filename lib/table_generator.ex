defmodule TableGenerator do
  @moduledoc """
  Generates a table with contents.
  """

  alias TableRex.Table

  @doc false
  @spec call(String.t, list(number), list(number)) :: Table.t
  def call(operator, x_operands, y_operands)
    when length(x_operands) > 0 and length(y_operands) > 0,
    do: generate_table(operator, x_operands, y_operands)

  defp generate_table(operator, x_operands, y_operands) do
    Table.new
    |> Table.add_row([operator] ++ x_operands)
    |> Table.add_rows(generate_rows(operator, x_operands, y_operands))
  end

  defp generate_rows(operator, x_nums, y_nums) do
    Enum.map(y_nums, fn(y) ->
      Enum.map(x_nums, fn(x) ->
        Operation.call(operator, [y, x])
      end) |> Enum.into([y])
    end)
  end
end
