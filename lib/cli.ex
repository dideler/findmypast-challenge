defmodule CLI do
  @moduledoc """
  Usage:

      sequence_table [options]

  ## Options

      -h, --help         # Shows this help and exits
      -v, --version      # Shows the program version and exits
      -n, --dimension N  # The NxN dimension of the table (default 10)
      -s, --sequence S   # The number sequence to use for the table axes (default "prime")
                         # Available options: "prime", "fibonacci"
      -o, --operator O   # The operator to use on the axes operands (default "*")
                         # Available options: "*"
  """

  @switches [help: :boolean, version: :boolean, dimension: :integer, sequence: :string, operator: :string]
  @aliases [h: :help, v: :version, n: :dimension, s: :sequence, o: :operator]

  @spec main(OptionParser.argv) :: :ok
  def main(args) do
    args |> parse_args |> validate_args! |> process
  end

  defp parse_args(args) do
    {opts, _} = OptionParser.parse!(args, strict: @switches, aliases: @aliases)

    cond do
      opts[:help] ->
        IO.puts @moduledoc
        System.halt(0)
      opts[:version] ->
        IO.puts Application.get_env(:sequence_table, :version)
        System.halt(0)
      true ->
        opts
    end
  end

  defp validate_args!(opts) do
    Enum.each opts, fn
      {:dimension, value} ->
        unless value > 0 do
          raise ArgumentError, "Expected a positive (i.e. greater than zero) dimension"
        end
      _ ->
        :ok
    end

    opts
  end

  defp process(opts) do
    dimension = Keyword.get(opts, :dimension, 10)
    sequence = Keyword.get(opts, :sequence, "prime")
    operator = Keyword.get(opts, :operator, "*")

    generator = SequenceGenerator.create(sequence)
    [x_operands, y_operands] = axes_operands(dimension, dimension, generator)

    TableGenerator.call(operator, x_operands, y_operands)
    |> TablePrinter.call(align: :right, intersections: :all)
  end

  defp axes_operands(x, y, generator)
    when is_integer(x) and is_integer(y) and x > 0 and y > 0 do
    {min, max} = Enum.min_max([x, y])
    x_axis = generator.first(max)
    y_axis = Enum.take(x_axis, min)
    if x > y, do: [x_axis, y_axis], else: [y_axis, x_axis]
  end
end
