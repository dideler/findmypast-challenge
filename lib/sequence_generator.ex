defmodule SequenceGenerator do
  @moduledoc "Behaviour for generating a number sequence"

  @type t :: module

  @doc "Generates the first n elements of a sequence"
  @callback first(non_neg_integer) :: list(integer)

  @doc "Factory function to return the appropriate sequence generator"
  @spec create(String.t) :: SequenceGenerator.t
  def create("prime"), do: EratosthenesGenerator
  def create("fibonacci"), do: FibonacciGenerator
  def create(sequence), do: raise ArgumentError, "Sequence not supported: #{sequence}"
end
