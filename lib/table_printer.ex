defmodule TablePrinter do
  @moduledoc """
  Handles decorating and displaying of a text table.
  """

  alias TableRex.Table

  @doc """
  Formats the given table and prints it to STDOUT.

  ## Options

    - `:title` - Assigns a title to the table. Defaults to no title.
    - `:align` - Aligns all body contents (excludes title).
      Accepts `:left`, `:center`, and `:right`. Defaults to left.
    - `:intersections` - Controls (horizontal) body separators.
      Accepts: `:all`, `:header`, `:frame`, and `:off`. Defaults to header.
  """
  @spec call(Table.t, [title: String.t, align: atom, intersections: atom]) :: :ok
  def call(table, opts \\ []) when is_list(opts) do
    table
    |> Table.put_title(opts[:title])
    |> Table.put_column_meta(:all, align: Keyword.get(opts, :align, :left))
    |> Table.render!(horizontal_style: Keyword.get(opts, :intersections, :header))
    |> IO.write
  end
end
