defmodule FibonacciGenerator do
  @moduledoc """
  Generates Fibonacci numbers recursively.
  """

  @behaviour SequenceGenerator

  @doc """
  Returns a list of the first `n` fibonacci numbers.

  ## Examples

    iex> FibonacciGenerator.first(5)
    [0, 1, 1, 2, 3]
  """
  @spec first(non_neg_integer) :: list(non_neg_integer)
  def first(0), do: []
  def first(n) when is_integer(n) and n < 0, do: raise ArgumentError, "Cannot generate negative size"
  def first(n) when is_integer(n) and n >= 0 do
    Enum.to_list(0..n-1) |> Enum.map(fn(x) -> fibonacci(x) end)
  end

  defp fibonacci(0), do: 0
  defp fibonacci(1), do: 1
  defp fibonacci(n), do: fibonacci(n-1) + fibonacci(n-2)
end
