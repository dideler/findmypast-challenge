defmodule EratosthenesGenerator do
  @moduledoc """
  Generates prime numbers using the "Sieve of Eratosthenes" algorithm.

  Starts with a table of numbers (e.g. 2, 3, 4, 5, ...) and progressively culls
  prime candidates by crossing off numbers that are a multiple of known primes,
  until the only numbers left are primes.
  """

  @behaviour SequenceGenerator

  @doc """
  Returns a list of the first `n` primes.

  ## Examples

    iex> EratosthenesGenerator.first(5)
    [2, 3, 5, 7, 11]
  """
  @spec first(non_neg_integer) :: list(pos_integer)
  def first(n) when is_integer(n) and n < 0, do: raise ArgumentError, "Cannot generate negative size"
  def first(n) when is_integer(n) and n >= 0 do
    generate_primes([], n)
  end

  defp generate_primes(primes, n) when n <= length(primes), do: Enum.take(primes, n)
  defp generate_primes(primes, n) do
    limit = estimated_upper_bound(n)
    sieve(limit, primes, candidates(limit)) |> generate_primes(n)
  end

  # https://en.wikipedia.org/wiki/Prime_number_theorem#Approximations_for_the_nth_prime_number
  # http://math.univ-lille1.fr/~ramare/TME-EMT/Articles/Art01.html
  defp estimated_upper_bound(n) when n <= 1, do: 2
  defp estimated_upper_bound(n) when n < 6 do
    n * (:math.log(n) + :math.log(:math.log(n + 8))) |> trunc
  end
  defp estimated_upper_bound(n) do
    n * (:math.log(n) + :math.log(:math.log(n))) |> trunc
  end

  defp sieve(_limit, primes, []), do: primes
  defp sieve(limit, primes, [prime_candidate | candidates]) do
    remaining_candidates = reject_multiples(candidates, prime_candidate, :math.sqrt(limit))
    sieve(limit, primes ++ [prime_candidate], remaining_candidates)
  end

  defp reject_multiples(candidates, factor, sqrt_n) when factor > sqrt_n, do: candidates
  defp reject_multiples(candidates, factor, _) do
    Enum.reject(candidates, fn(x) -> rem(x, factor) == 0 end)
  end

  defp candidates(upper_bound) when upper_bound < 2, do: []
  defp candidates(upper_bound) do
    Enum.to_list(2..upper_bound)
  end
end
