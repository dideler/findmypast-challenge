defmodule Operation do
  @moduledoc """
  Executes a mathematical calculation given an operator and operands.
  """

  def call("*", operands), do: Enum.reduce(operands, fn(x, acc) -> x * acc end)
  def call(operator, _operands), do: raise ArgumentError, "Operator not supported: #{operator}"
end
