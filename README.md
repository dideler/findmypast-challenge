# Sequence Table

## Task

Write an application that takes numeric input _n_ from a user,
and outputs a multiplication table of _n_ prime numbers.

**Requirements:**

- You must use Elixir (normally it's language agnostic, but I've done this challenge before)

- Do not use a brute force / trial division algorithm

- Include a test for a performant prime algorithm that can generate a list of 20,000+ primes

- Small git commits so that we can see how you've developed your solution

- Write your application with high unit test coverage

- For the input and output you can use the console, a web page, or something else

- You cannot use an existing library to generate primes, must write it yourself

- The user should input a whole number _n_, where is _n_ is at least 1

- The application should output an _n+1 x n+1_ grid of numbers

## Getting Started

### Installation

Install the Elixir version specified in [mix.exs](mix.exs).

Install dependencies with `mix deps.get`

### Running the app

Generate the sequence_table executable with `mix escript.build`

Then run `./sequence_table -h` to see the help or try running it without arguments

### Tests

Run tests with `mix test`

Run benchmarks with `mix bench`

### Docs

Generate documentation with `MIX_ENV=docs mix docs`

Generate a documentation analysis report with `MIX_ENV=docs mix inch`

## Notes

### What I'm pleased with

Considering this is my first Elixir application, I'm pleased that I was able to implement
it while still following best practices. TDD'd as much as possible. Modules aren't too tightly coupled.
Often referred to the Elixir community style guide so my code is more idiomatic.
I read a lot of Elixir code (mainly from Phoenix and Elixir) to see how they approached certain problems
(e.g. argument parsing). I used patterns where they made sense (e.g. Factory, Command, Behaviour).

### What I'd consider doing with more time

_Disclaimer: I spent a significant amount of time on this challenge (evident by commit history).
This is the first Elixir application I've written, and the first application in a functional language.
I really enjoyed it and tried to dive in deep. But I underestimated the learning time._

- Increase confidence that generated primes are correct, for example, the first million.
  For such a large set we would need to keep expected primes in a support file that we load during testing,
  or compare results with a trusted 3rd-party library (only loaded in test environment).

- Add a `prime?/1` function for checking whether a specific number is prime.
  Would start with a simple [primality test](https://en.wikipedia.org/wiki/Primality_test).

- Improve performance for generating primes. E.g. Use a more efficient sieve, or generate primes concurrently.

- The Sieve of Eratosthenes is good for generating primes **up to** _n_, but not for generating the
  **first** _n_ as the upper limit doesn't determine the number of primes in the resulting set.
  I took the approach of [estimating a sufficiently tight upperbound][wiki-upperbound] for the sieve
  which worked out well, but a more efficient approach may be to use a
  [segmented or incremental sieve][wiki-incremental] if possible.

  [wiki-upperbound]: https://en.wikipedia.org/wiki/Prime_number_theorem#Approximations_for_the_nth_prime_number
  [wiki-incremental]: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes#Incremental_sieve

- Those familiar with the details of the Sieve of Eratosthenes may notice that my implementation isn't
  following the algorithm's precise definition. My algorithm is iteratively filtering out the composites
  through divisibility testing by sequential primes, one prime at a time.  
  Turns out it's common to confuse this "unfaithful sieve" with the Eratosthenes algorithm as I've done,
  especially in functional languages where it's simple and looks like a very elegant solution. The difference
  is in what gets crossed off, when, and how many times, as outlined in
  _[The Genuine Sieve of Eratosthenes][real-sieve]_ by Melissa E. O'Neill.  
  I noticed this mistake when focusing on performance, at that point I had spent a considerate amount of time
  on prime generation already and left it as is to continue to other areas. At FC, we didn't have a
  restriction on the algorithm you could use, so this distinction wasn't obvious to me.
  With more time I would attempt to implement the proper algorithm as specified by Melissa.

  [real-sieve]: https://www.cs.hmc.edu/~oneill/papers/Sieve-JFP.pdf

- Some unit tests are also integration testing. Not much of a concern for this small app as performance-wise
  the test suite is still very quick. But if we wanted to have stricter boundaries in tests, could start
  introducing stubs and mocks in our unit tests. Then create a separate directory for integration tests.

- Namespace every module with the app name as is convention, e.g. `MyApp.SomeModule`. Will reduce chance of
  naming conflicts, but can also be a neater way to organises modules.

- Use a static analysis tool like dialyzer to warn about code problems (e.g. type mismatches). With the aid
  of specs, this would also allow us to reduce the number of guard clauses which I've used very defensively.
  Guard clauses are more idiomatic when used to improve pattern matching instead of raising early.
  A `FunctionClauseError` is less descriptive than a `ArithmeticError` for example.
