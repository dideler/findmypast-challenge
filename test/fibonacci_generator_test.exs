defmodule FibonacciGeneratorTest do
  use ExUnit.Case, async: true
  doctest FibonacciGenerator

  test "first/1 raises when given a negative integer" do
    assert_raise ArgumentError, "Cannot generate negative size", fn ->
      FibonacciGenerator.first(-1)
    end
  end

  test "first/1 generates the first zero fibonacci numbers" do
    assert FibonacciGenerator.first(0) == []
  end

  test "first/1 generates the first 20 fibonacci numbers" do
    assert FibonacciGenerator.first(20) == [
      0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181
    ]
  end
end
