defmodule CLITest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureIO

  test "raises if not given a positive dimension" do
    assert_raise ArgumentError, "Expected a positive (i.e. greater than zero) dimension", fn ->
      CLI.main(["-n", "0"])
    end
  end

  test "prints a table using default options" do
    assert capture_io(fn -> CLI.main([]) end) == """
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  * |  2 |  3 |   5 |   7 |  11 |  13 |  17 |  19 |  23 |  29 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  2 |  4 |  6 |  10 |  14 |  22 |  26 |  34 |  38 |  46 |  58 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  3 |  6 |  9 |  15 |  21 |  33 |  39 |  51 |  57 |  69 |  87 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  5 | 10 | 15 |  25 |  35 |  55 |  65 |  85 |  95 | 115 | 145 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  7 | 14 | 21 |  35 |  49 |  77 |  91 | 119 | 133 | 161 | 203 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 11 | 22 | 33 |  55 |  77 | 121 | 143 | 187 | 209 | 253 | 319 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 13 | 26 | 39 |  65 |  91 | 143 | 169 | 221 | 247 | 299 | 377 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 17 | 34 | 51 |  85 | 119 | 187 | 221 | 289 | 323 | 391 | 493 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 19 | 38 | 57 |  95 | 133 | 209 | 247 | 323 | 361 | 437 | 551 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 23 | 46 | 69 | 115 | 161 | 253 | 299 | 391 | 437 | 529 | 667 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 29 | 58 | 87 | 145 | 203 | 319 | 377 | 493 | 551 | 667 | 841 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    """
  end

  test "dimension" do
    assert capture_io(fn -> CLI.main(["-n", "2"]) end) == """
    +---+---+---+
    | * | 2 | 3 |
    +---+---+---+
    | 2 | 4 | 6 |
    +---+---+---+
    | 3 | 6 | 9 |
    +---+---+---+
    """
  end

  test "sequence" do
    assert capture_io(fn -> CLI.main(["-s", "prime"]) end) == """
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  * |  2 |  3 |   5 |   7 |  11 |  13 |  17 |  19 |  23 |  29 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  2 |  4 |  6 |  10 |  14 |  22 |  26 |  34 |  38 |  46 |  58 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  3 |  6 |  9 |  15 |  21 |  33 |  39 |  51 |  57 |  69 |  87 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  5 | 10 | 15 |  25 |  35 |  55 |  65 |  85 |  95 | 115 | 145 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  7 | 14 | 21 |  35 |  49 |  77 |  91 | 119 | 133 | 161 | 203 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 11 | 22 | 33 |  55 |  77 | 121 | 143 | 187 | 209 | 253 | 319 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 13 | 26 | 39 |  65 |  91 | 143 | 169 | 221 | 247 | 299 | 377 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 17 | 34 | 51 |  85 | 119 | 187 | 221 | 289 | 323 | 391 | 493 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 19 | 38 | 57 |  95 | 133 | 209 | 247 | 323 | 361 | 437 | 551 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 23 | 46 | 69 | 115 | 161 | 253 | 299 | 391 | 437 | 529 | 667 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 29 | 58 | 87 | 145 | 203 | 319 | 377 | 493 | 551 | 667 | 841 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    """
  end

  test "operator" do
    assert capture_io(fn -> CLI.main(["-o", "*"]) end) == """
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  * |  2 |  3 |   5 |   7 |  11 |  13 |  17 |  19 |  23 |  29 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  2 |  4 |  6 |  10 |  14 |  22 |  26 |  34 |  38 |  46 |  58 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  3 |  6 |  9 |  15 |  21 |  33 |  39 |  51 |  57 |  69 |  87 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  5 | 10 | 15 |  25 |  35 |  55 |  65 |  85 |  95 | 115 | 145 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    |  7 | 14 | 21 |  35 |  49 |  77 |  91 | 119 | 133 | 161 | 203 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 11 | 22 | 33 |  55 |  77 | 121 | 143 | 187 | 209 | 253 | 319 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 13 | 26 | 39 |  65 |  91 | 143 | 169 | 221 | 247 | 299 | 377 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 17 | 34 | 51 |  85 | 119 | 187 | 221 | 289 | 323 | 391 | 493 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 19 | 38 | 57 |  95 | 133 | 209 | 247 | 323 | 361 | 437 | 551 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 23 | 46 | 69 | 115 | 161 | 253 | 299 | 391 | 437 | 529 | 667 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    | 29 | 58 | 87 | 145 | 203 | 319 | 377 | 493 | 551 | 667 | 841 |
    +----+----+----+-----+-----+-----+-----+-----+-----+-----+-----+
    """
  end
end
