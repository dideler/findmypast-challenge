defmodule OperationTest do
  use ExUnit.Case, async: true

  test "multiplication command" do
    assert Operation.call("*", [1, 2, 3]) == 6
  end

  test "raises when operator unknown" do
    assert_raise ArgumentError, "Operator not supported: /", fn ->
      Operation.call("/", [10, 2])
    end
  end
end
