defmodule TableGeneratorTest do
  use ExUnit.Case, async: true

  describe "guard clauses" do
    test "raises when given an empty list of operands" do
      assert_raise FunctionClauseError, ~r{TableGenerator.call/3}, fn ->
        TableGenerator.call("*", [], [1])
      end

      assert_raise FunctionClauseError, ~r{TableGenerator.call/3}, fn ->
        TableGenerator.call("*", [1], [])
      end
    end
  end

  test "generates a table with correct contents" do
    rows = [
      [
        %TableRex.Cell{value: "3"},
        %TableRex.Cell{value: "6"},
        %TableRex.Cell{value: "9"}
      ],
      [
        %TableRex.Cell{value: "2"},
        %TableRex.Cell{value: "4"},
        %TableRex.Cell{value: "6"}
      ],
      [
        %TableRex.Cell{value: "*"},
        %TableRex.Cell{value: "2"},
        %TableRex.Cell{value: "3"}
      ]
    ]
    assert TableGenerator.call("*", [2, 3], [2, 3]) == %TableRex.Table{rows: rows}
  end

  describe "dimensions" do
    test "generates a 1x1 table" do
      %{ rows: rows } = TableGenerator.call("*", [2], [2])
      assert length(rows) == 2
      assert Enum.all?(rows, fn(row) -> length(row) == 2 end)
    end

    test "generates a 2x4 table" do
      %{ rows: rows } = TableGenerator.call("*", [2, 3], [2, 3, 5, 7])
      assert length(rows) == 5
      assert Enum.all?(rows, fn(row) -> length(row) == 3 end)
    end

    test "can print a 4x2 table" do
      %{ rows: rows } = TableGenerator.call("*", [2, 3, 5, 7], [2, 3])
      assert length(rows) == 3
      assert Enum.all?(rows, fn(row) -> length(row) == 5 end)
    end
  end
end
