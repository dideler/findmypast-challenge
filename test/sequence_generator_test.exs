defmodule SequenceGeneratorTest do
  use ExUnit.Case, async: true

  describe "create/1" do
    test "supports 'prime' sequence" do
      assert SequenceGenerator.create("prime") == EratosthenesGenerator
    end

    test "support 'fibonacci' sequence" do
      assert SequenceGenerator.create("fibonacci") == FibonacciGenerator
    end

    test "raises when sequence unsupported" do
      assert_raise ArgumentError, "Sequence not supported: composite", fn ->
        SequenceGenerator.create("composite")
      end
    end
  end
end
