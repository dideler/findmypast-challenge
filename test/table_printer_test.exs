defmodule TablePrinterTest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureIO

  setup_all do
    { :ok, table: TableGenerator.call("*", [2, 3, 5], [2, 3, 5]) }
  end

  test "raises if options not given in a keyword list", %{table: table} do
    assert_raise FunctionClauseError, ~r{TablePrinter.call/2}, fn ->
      TablePrinter.call(table, %{title: "Raise the roof"})
    end
  end

  test "prints a table with default formatting", %{table: table} do
    assert capture_io(fn -> TablePrinter.call(table) end) == """
    +---+----+----+----+
    | * | 2  | 3  | 5  |
    | 2 | 4  | 6  | 10 |
    | 3 | 6  | 9  | 15 |
    | 5 | 10 | 15 | 25 |
    +---+----+----+----+
    """
  end

  describe "special formatting" do
    test "with a title", %{table: table} do
      assert capture_io(fn -> TablePrinter.call(table, title: "A Table") end) == """
      +------------------+
      |     A Table      |
      +---+----+----+----+
      | * | 2  | 3  | 5  |
      | 2 | 4  | 6  | 10 |
      | 3 | 6  | 9  | 15 |
      | 5 | 10 | 15 | 25 |
      +---+----+----+----+
      """
    end

    test "align columns", %{table: table} do
      assert capture_io(fn -> TablePrinter.call(table, align: :right) end) == """
      +---+----+----+----+
      | * |  2 |  3 |  5 |
      | 2 |  4 |  6 | 10 |
      | 3 |  6 |  9 | 15 |
      | 5 | 10 | 15 | 25 |
      +---+----+----+----+
      """
    end

    test "body intersections", %{table: table} do
      assert capture_io(fn -> TablePrinter.call(table, intersections: :all) end) == """
      +---+----+----+----+
      | * | 2  | 3  | 5  |
      +---+----+----+----+
      | 2 | 4  | 6  | 10 |
      +---+----+----+----+
      | 3 | 6  | 9  | 15 |
      +---+----+----+----+
      | 5 | 10 | 15 | 25 |
      +---+----+----+----+
      """
    end
  end
end
