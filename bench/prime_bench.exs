defmodule PrimeBench do
  use Benchfella

  bench "first 20,000 primes using EratosthenesGenerator" do
    EratosthenesGenerator.first(20_000)
  end
end
