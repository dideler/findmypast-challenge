defmodule SequenceTable.Mixfile do
  use Mix.Project

  def project do
    [
      app: :sequence_table,
      name: "Sequence Table",
      version: "0.1.0",
      elixir: "~> 1.4",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps(),
      escript: [main_module: CLI],
      docs: [
        extras: ["README.md"],
        main: "readme",
        source_url: "https://gitlab.com/dideler/findmypast-challenge",
      ],
   ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:logger, :table_rex]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      {:benchfella, "~> 0.3.4"},
      {:table_rex, "~> 0.10.0"},
      {:dialyxir, "~> 0.5", only: :dev, runtime: false},
      {:ex_doc, "~> 0.15", only: :docs, runtime: false},
      {:inch_ex, "~> 0.5", only: :docs, runtime: false},
    ]
  end
end
